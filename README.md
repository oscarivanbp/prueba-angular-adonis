# Adonis API application

primero realice el servicio api en un nuevo proyecto, este lo realice en la version `4.1.0` de adonis.
para crear este servicio configure un gestor de base de datos en mysql el cual esta en el archivo .env con el nombre de la base de datos `AdonisPrueba` en formato `utf8_spanish2_ci`. 
esta base de datos debemos inicializarla para ello utilizamos mysql y creamos la base de datos "AdonisPrueba" no necesitamos crear tablas ya que estas las migraremos de los modelos creados en nuestra aplicación.

Ahora creamos los modelos, los controladores y las migraciones en base a nuestro modelo relacional y modificamos el metodo up() de  nuestras migraciones.

luego simplemente corremos nuestras migraciones y ya con esto en nuestra base de datos se crean las tablas que declaramos en nuestras migraciones.

procedemos entonces a crear las rutas para el consumo de esta api, para esto nos vamos a routes y acá creamos estas rutas las cuales conectan a nuestros controladores donde creamos los metodos para devolver los datos solicitados por el cliente de nuestra api, para registrar a un nuevo usuario o para verificar los datos del login en nuestro cliente angular.



# Angular application

Este cliente de nuestra api lo realice en angular version `11.2.9`

en donde lo primero a realizar fue el registro de los usuarios para ello creamos un nuevo componente y creamos el formulario de registro en html y utilizamos typescrypt para poder extraer todos estos datos del formulario y procesarlos, nos ayudamos de un modelo y luego de un servicio para poder mandar estos datos a nuestro backend.

hacemos lo mismo con el login y con las interfaces que creamos son necesarias.


# Que encontramos
al lanzar nuestra aplicación frontend nos encontramos con un formulario de ingreso si ya tenemos usuario. sino podemos registrarnos en el apartado ¿no tienes cuenta aún?.
acá llenamos todos los datos, todos son obligatorios y validados por nuestras interfaces, nos registramos y luego pasamos a hacer el ingreso a la aplicación 

Cuando ingresamos el nos desplega una vista donde opdemos ingresar el salario y solicitarle al nuestra api nos devuelva 3 posibles rangos en el cual en uno de ellos se encuentra el ingresado por nosotros y los ordena aleatoriamente desde nuestra api, acá solo nos encargamos de visualizar estos datos.

tambien podemos ver un menú donde podemos navegar a la opcion aleatorio o cerrar sesion 

si vamos a la opción aleatorio nos muestra información de nuestro usuario ingresado como su nombre, su correo electronico y su salario 
acá podemos ingresar un rango, este rango es enviado al servidor junto con el salario donde por medio de las funciones de aleatoriedad math.random, math.ceil, math.floor creamos unos rangos aleatoriamente y verificamos que el salario del usuario ingresado al sistema este dentro de estos rangos y pasamos a devolver esta informacion del backend al frontend y luego listarla en nuestra aplicación.

# Estrategias 

- Tratar de mantener un orden con todos los elementos y llamarlos de manera descriptiva para luego no perder tiempo pensado en que hacia cada cosa.
- antes de comenzar con el backend tener un modelo relacional de la bd y así poder hacer las migraciones mas facilemnte.
- utilizar siempre un modelo de datos para cada formulario y así facilitar el manejo de estos datos.

# Dificultades

1. los CORS para ello solo fui a la documentación y lo solucione rapidamente.
2. diseñar un formulario visualmente agradable.
3. el llamado de las variables y funciones que se realiza con un this, en varias ocasiones solo llamaba un metodo sin este this. 
    al comienzo fue un problema pero al final uno ya se acuerda de como realizar estos llamados.



