export interface Users {
  user_id: number;
  fullname: string;
  cellphone: number;
  email: string;
}
