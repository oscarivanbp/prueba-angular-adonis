export interface UsersFinancial {
  user_id_financial: number;
  salary: number;
  other_income: number;
  monthly_expenses: number;
  financial_expenses: number;
}
