export interface UsersHome {
  user_id_home: number;
  department: string;
  city: string;
  neighborhood: string;
  address: string;
}
