export interface User {
  name: string;
  id: number;
  cellphone: number;
  email: string;
  password: string;
  department: string;
  city: string;
  neighborhood: string;
  address: string;
  salary: number;
  other_income: number;
  monthly_expenses: number;
  financial_expenses: number;
}
