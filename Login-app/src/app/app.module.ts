import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './component/auth/login/login.component';
import { RegisterComponent } from './component/auth/register/register.component';
import { HomeComponent } from './component/main/home/home.component';
import { CalculationsComponent } from './component/operations/calculations/calculations.component';
import { OperationService } from './services/operation.service';
import { AuthService } from './services/auth.service';
import { ProcedureComponent } from './component/operations/procedure/procedure.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    CalculationsComponent,
    ProcedureComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [AuthService, OperationService],
  bootstrap: [AppComponent],
})
export class AppModule {}
