import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import {
  errorMessage,
  successDialog,
  timeMessage,
} from 'src/app/functions/alerts';
import { User } from 'src/app/models/user';
import { Users } from 'src/app/models/users';
import { UsersFinancial } from 'src/app/models/users-financial';
import { UsersHome } from 'src/app/models/users-home';
import { UsersLogin } from 'src/app/models/users-login';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup = new FormGroup({});
  user!: User;
  users!: Users;
  usersFinancial!: UsersFinancial;
  usersHome!: UsersHome;
  usersLogin!: UsersLogin;

  //new FormControl('Default value');

  constructor(
    private fb: FormBuilder,
    private AuthService: AuthService,
    private router: Router
  ) {
    this.createFrom();
  }

  ngOnInit(): void {}

  register(): void {
    //console.log(this.registerForm.value);
    if (this.registerForm.invalid) {
      return Object.values(this.registerForm.controls).forEach((cnt) => {
        cnt.markAsTouched();
      });
    } else {
      this.setUsers();
      this.setUsersFinancial();
      this.setUsersHome();
      this.setUsersLogin();
      this.AuthService.registerUser(this.users).subscribe(
        (data: any) => {},
        (error) => {
          errorMessage('Error');
        }
      );
      this.AuthService.registerUserFinancial(this.usersFinancial).subscribe(
        (data: any) => {},
        (error) => {
          errorMessage('Error');
        }
      );
      this.AuthService.registerUserHome(this.usersHome).subscribe(
        (data: any) => {},
        (error) => {
          errorMessage('Error');
        }
      );
      this.AuthService.registerUserLogin(this.usersLogin).subscribe(
        (data: any) => {
          timeMessage('Registrando', 1500).then(() => {
            successDialog('Registro Completado');
            this.router.navigate(['/login']);
          });
        },
        (error) => {
          errorMessage('Error');
        }
      );
    }
  }

  createFrom(): void {
    this.registerForm = this.fb.group({
      name: ['', [Validators.required]],
      id: ['', [Validators.required]],
      cellphone: ['', [Validators.required]],
      email: [
        '',
        [
          Validators.required,
          Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$'),
        ],
      ],
      password: ['', [Validators.required]],
      password2: ['', [Validators.required]],
      department: ['', [Validators.required]],
      city: ['', [Validators.required]],
      neighborhood: ['', [Validators.required]],
      address: ['', [Validators.required]],
      salary: ['', [Validators.required]],
      other_income: ['', [Validators.required]],
      monthly_expenses: ['', [Validators.required]],
      financial_expenses: ['', [Validators.required]],
    });
  }

  get nameValidate() {
    return (
      this.registerForm.get('name')?.invalid &&
      this.registerForm.get('name')?.touched
    );
  }

  get idValidate() {
    return (
      this.registerForm.get('id')?.invalid &&
      this.registerForm.get('id')?.touched
    );
  }
  get cellValidate() {
    return (
      this.registerForm.get('cellphone')?.invalid &&
      this.registerForm.get('cellphone')?.touched
    );
  }

  get emailValidate() {
    return (
      this.registerForm.get('email')?.invalid &&
      this.registerForm.get('email')?.touched
    );
  }

  get passwordValidate() {
    return (
      this.registerForm.get('password')?.invalid &&
      this.registerForm.get('password')?.touched
    );
  }

  get password2Validate() {
    const pass = this.registerForm.get('password')?.value;
    const pass2 = this.registerForm.get('password2')?.value;
    return pass === pass2 ? false : true;
  }

  get departmentValidate() {
    return (
      this.registerForm.get('department')?.invalid &&
      this.registerForm.get('department')?.touched
    );
  }

  get cityValidate() {
    return (
      this.registerForm.get('city')?.invalid &&
      this.registerForm.get('city')?.touched
    );
  }

  get neighborhoodValidate() {
    return (
      this.registerForm.get('neighborhood')?.invalid &&
      this.registerForm.get('neighborhood')?.touched
    );
  }

  get addressValidate() {
    return (
      this.registerForm.get('address')?.invalid &&
      this.registerForm.get('address')?.touched
    );
  }

  get salaryValidate() {
    return (
      this.registerForm.get('salary')?.invalid &&
      this.registerForm.get('salary')?.touched
    );
  }

  get other_incomeValidate() {
    return (
      this.registerForm.get('other_income')?.invalid &&
      this.registerForm.get('other_income')?.touched
    );
  }

  get monthly_expensesValidate() {
    return (
      this.registerForm.get('monthly_expenses')?.invalid &&
      this.registerForm.get('monthly_expenses')?.touched
    );
  }

  get financial_expensesValidate() {
    return (
      this.registerForm.get('financial_expenses')?.invalid &&
      this.registerForm.get('financial_expenses')?.touched
    );
  }

  setUsers(): void {
    this.users = {
      user_id: this.registerForm.get('id')?.value,
      fullname: this.registerForm.get('name')?.value,
      cellphone: this.registerForm.get('cellphone')?.value,
      email: this.registerForm.get('email')?.value,
    };
  }
  setUsersFinancial(): void {
    this.usersFinancial = {
      user_id_financial: this.registerForm.get('id')?.value,
      salary: this.registerForm.get('salary')?.value,
      other_income: this.registerForm.get('other_income')?.value,
      monthly_expenses: this.registerForm.get('monthly_expenses')?.value,
      financial_expenses: this.registerForm.get('financial_expenses')?.value,
    };
  }
  setUsersHome(): void {
    this.usersHome = {
      user_id_home: this.registerForm.get('id')?.value,
      department: this.registerForm.get('department')?.value,
      city: this.registerForm.get('city')?.value,
      neighborhood: this.registerForm.get('neighborhood')?.value,
      address: this.registerForm.get('address')?.value,
    };
  }
  setUsersLogin(): void {
    this.usersLogin = {
      email_login: this.registerForm.get('email')?.value,
      password: this.registerForm.get('password')?.value,
    };
  }
}
