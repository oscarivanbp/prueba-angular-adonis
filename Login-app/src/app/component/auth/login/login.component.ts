import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {
  errorMessage,
  successDialog,
  timeMessage,
} from 'src/app/functions/alerts';
import { UserLogin } from 'src/app/models/user-login';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = new FormGroup({});
  userLogin!: UserLogin;

  constructor(
    private form: FormBuilder,
    private AuthService: AuthService,
    private router: Router
  ) {
    this.createForm();
  }

  ngOnInit(): void {}

  login(): void {
    if (this.loginForm.invalid) {
      return Object.values(this.loginForm.controls).forEach((cnt) => {
        cnt.markAsTouched();
      });
    } else {
      this.setUser();
      this.AuthService.login(this.userLogin).subscribe(
        (data: any) => {
          timeMessage('Iniciando ', 1500).then(() => {
            successDialog('iniciado').then(() => {
              this.router.navigate(['/procedure']);
            });
          });
        },
        (error) => {
          errorMessage('Usuario o contraseña Incorrecta');
        }
      );
    }
  }

  get emailValidate() {
    return (
      this.loginForm.get('email')?.invalid &&
      this.loginForm.get('email')?.touched
    );
  }

  get passwordValidate() {
    return (
      this.loginForm.get('password')?.invalid &&
      this.loginForm.get('password')?.touched
    );
  }

  createForm(): void {
    this.loginForm = this.form.group({
      email: [
        '',
        [
          Validators.required,
          Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$'),
        ],
      ],
      password: ['', [Validators.required]],
    });
  }

  setUser(): void {
    this.userLogin = {
      email: this.loginForm.get('email')?.value,
      password: this.loginForm.get('password')?.value,
    };
  }
}
