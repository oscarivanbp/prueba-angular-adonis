import { Component, OnInit } from '@angular/core';

import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { OperationService } from 'src/app/services/operation.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-calculations',
  templateUrl: './calculations.component.html',
  styleUrls: ['./calculations.component.scss'],
})
export class CalculationsComponent implements OnInit {
  datos: any = [];
  datos1: any = [];
  rangos: any = [];

  Form: FormGroup = new FormGroup({});

  operationsForm: FormGroup = new FormGroup({});

  constructor(
    private fb: FormBuilder,
    private operationService: OperationService,
    private router: Router
  ) {
    this.createFromEmpty();
  }

  public openClose() {
    var rangeMax = this.Form.get('range')?.value;
    if (this.Form.valid) {
      console.log(this.Form.get('salary')?.value);
      console.log(this.Form.get('range')?.value);

      this.operationService
        .getRangeSalary(this.Form.get('salary')?.value, rangeMax)
        .subscribe(
          (res) => (
            console.log(res), (this.rangos = res), this.createDataFrom()
          ),

          (err) => console.log(err)
        );
    } else {
    }
  }

  onLoadBody() {
    this.router.navigate(['/procedure']);
  }

  logOff() {
    window.localStorage.clear();
    this.router.navigate(['/login']);
  }

  ngOnInit(): void {
    if (window.localStorage.getItem('email') != null) {
      this.datos1 = window.localStorage.getItem('email');
      this.operationService.getUsers(this.datos1).subscribe(
        (res) => ((this.datos = res), this.createFrom()),

        (err) => console.log(err)
      );
    } else {
      console.log('no data');
      this.router.navigate(['/login']);
    }
  }
  createFromEmpty(): void {
    this.Form = this.fb.group({
      input: [''],
      na: [''],
      email: [''],
      salary: [''],
      range: [''],
      A0: [''],
      A1: [''],
      B0: [''],
      B1: [''],
      C0: [''],
      C1: [''],
      op: [''],
    });
  }

  createFrom(): void {
    console.log('A');

    this.Form = this.fb.group({
      input: [''],
      na: [this.datos[0]['fullname'], []],
      email: [this.datos[0]['email']],
      salary: [this.datos[0]['salary']],
      range: ['', [Validators.required]],
    });
  }

  createDataFrom(): void {
    console.log('A');

    this.Form = this.fb.group({
      salary: [this.Form.get('salary')?.value],
      A0: [this.rangos[0]['min']],
      A1: [this.rangos[0]['max']],
      B0: [this.rangos[1]['min']],
      B1: [this.rangos[1]['max']],
      C0: [this.rangos[2]['min']],
      C1: [this.rangos[2]['max']],
      op: [this.rangos[3]['option']],
      range: [this.Form.get('range')?.value, [Validators.required]],
    });
  }

  get rangeValidate() {
    return this.Form.get('range')?.invalid && this.Form.get('range')?.touched;
  }
}
