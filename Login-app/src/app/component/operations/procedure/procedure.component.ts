import { Component, OnInit } from '@angular/core';

import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { OperationService } from 'src/app/services/operation.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-procedure',
  templateUrl: './procedure.component.html',
  styleUrls: ['./procedure.component.scss'],
})
export class ProcedureComponent implements OnInit {
  Form: FormGroup = new FormGroup({});
  range: any = [];

  constructor(
    private fb: FormBuilder,
    private operationService: OperationService,
    private router: Router
  ) {
    this.createFrom();
  }

  open() {
    if (this.Form.valid) {
      this.operationService
        .getRangeSalaryProcedure(this.Form.get('salary')?.value)
        .subscribe(
          (res) => (
            console.log(res), (this.range = res), this.createFromData()
          ),

          (err) => console.log(err)
        );
    } else {
    }
  }

  logOff() {
    window.localStorage.clear();
    this.router.navigate(['/login']);
  }

  onLoadBody() {
    this.router.navigate(['/procedure']);
  }

  ngOnInit(): void {
    this.createFrom;
    if (window.localStorage.getItem('email') != null) {
      //this.createFrom();
    } else {
      console.log('no data');
      this.router.navigate(['/login']);
    }
  }

  createFrom(): void {
    this.Form = this.fb.group({
      salary: ['', [Validators.required]],
      A0: [''],
      A1: [''],
      B0: [''],
      B1: [''],
      C0: [''],
      C1: [''],
      op: [''],
    });
  }

  createFromData(): void {
    this.Form = this.fb.group({
      salary: [this.Form.get('salary')?.value, [Validators.required]],
      A0: [this.range[0]['min']],
      A1: [this.range[0]['max']],
      B0: [this.range[1]['min']],
      B1: [this.range[1]['max']],
      C0: [this.range[2]['min']],
      C1: [this.range[2]['max']],
      op: [this.range[3]['option']],
    });
  }

  get salaryValidate() {
    return this.Form.get('salary')?.invalid && this.Form.get('salary')?.touched;
  }
}
