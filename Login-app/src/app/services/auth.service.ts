import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { Users } from 'src/app/models/users';
import { UserLogin } from 'src/app/models/user-login';
import { UsersFinancial } from '../models/users-financial';
import { UsersHome } from '../models/users-home';
import { UsersLogin } from '../models/users-login';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  apiURL = environment.apiURL;

  constructor(private http: HttpClient) {}

  registerUser(users: Users): Observable<any> {
    return this.http.post(`${this.apiURL}users`, users);
  }
  registerUserFinancial(usersFinancial: UsersFinancial): Observable<any> {
    return this.http.post(`${this.apiURL}user_financial`, usersFinancial);
  }
  registerUserHome(usersHome: UsersHome): Observable<any> {
    return this.http.post(`${this.apiURL}user_home`, usersHome);
  }
  registerUserLogin(usersLogin: UsersLogin): Observable<any> {
    return this.http.post(`${this.apiURL}user_login`, usersLogin);
  }

  login(userLogin: UserLogin): Observable<any> {
    window.localStorage.setItem('email', userLogin.email);
    return this.http.post(`${this.apiURL}log`, userLogin);
  }
}
