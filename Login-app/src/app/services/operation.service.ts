import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { Observable } from 'rxjs';
import { UsersLogin } from 'src/app/models/users-login';

@Injectable({
  providedIn: 'root',
})
export class OperationService {
  apiURL = environment.apiURL;

  constructor(private http: HttpClient) {}

  getUserAll() {
    return this.http.get(`${this.apiURL}getDataAll`);
  }

  getUsers(email: string): Observable<any> {
    return this.http.post(`${this.apiURL}getData/`, { email: email });
  }

  getRangeSalary(salary: number, range: number): Observable<any> {
    return this.http.post(`${this.apiURL}getRangeSalary/`, {
      salary: salary,
      range: range,
    });
  }

  getRangeSalaryProcedure(salary: number): Observable<any> {
    return this.http.post(`${this.apiURL}getRangeSalaryPro/`, {
      salary: salary,
    });
  }
}
