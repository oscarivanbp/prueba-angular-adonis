'use strict'
const { formatters } = use('Validator')
class UsersLogin {
    get rules() {
        return {
            user_id_login: 'required|unique:users.user_id',
            email_login: 'required|email|max:180|unique:users.email',
            password: 'required|max:250'
        }
    }


    get validateAll() {
        return true
    }

    get formatter() {
        return formatters.JsonApi
    }
}

module.exports = UsersLogin