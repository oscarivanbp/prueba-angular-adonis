'use strict'
const { formatters } = use('Validator')
class UsersHome {
    get rules() {
        return {
            user_id_financial: 'required|unique:users.user_id',
            salary: 'required|max:250',
            other_income: 'required|max:250',
            monthly_expenses: 'required|',
            financial_expenses: 'required|'

        }
    }


    get validateAll() {
        return true
    }

    get formatter() {
        return formatters.JsonApi
    }
}

module.exports = UsersHome