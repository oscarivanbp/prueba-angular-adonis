'use strict'
const { formatters } = use('Validator')

class Users {
    get rules() {
        return {
            user_id: 'required|unique:users.user_id',
            fullname: 'required|max:250',
            cellphone: 'required|max:100',
            email: 'required|email|max:180|unique:users.email'

        }
    }

    get validateAll() {
        return true
    }

    get formatter() {
        return formatters.JsonApi
    }

}

module.exports = LoginUser