'use strict'


const SaveLogin = use('App/Models/SaveLogin')
class SaveLoginController {
    async store({ request, response }) {
        const UserData = request.only(['user_id_login', 'email_login', 'password'])
        const Log = await SaveLogin.create(UserData)

        return response.created({
            status: true,
            data: Log
        })
    }

    async login({ request, response, auth }) {

        const { email, password } = request.only(['email', 'password'])

        const Database = use('Database')
        const posts = await Database
            .table('save_logins')
            .where('email_login', email)
        for (let i in posts) {
            var per = {
                "id": posts[i].id,
                "email_login": posts[i].email_login,
                "password": posts[i].password,
            }

        }
        if (per.password == password && per.email_login == email) {
            return response.ok();

        } else {
            login();

        }
    }



}

module.exports = SaveLoginController