'use strict'
const User = use('App/Models/user')
class UserController {

    async store({ request, response }) {
        const UserData = request.only(['user_id', 'fullname', 'cellphone', 'email'])
        const user = await User.create(UserData)

        return response.created({
            status: true,
            data: user
        })
    }
}

module.exports = UserController