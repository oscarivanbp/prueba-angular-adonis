'use strict'
const UserHome = use('App/Models/UserHome')
class UserHomeController {
    async store({ request, response }) {
        const UserData = request.only(['user_id_home', 'department', 'city', 'neighborhood', 'address'])
        const user = await UserHome.create(UserData)

        return response.created({
            status: true,
            data: user
        })
    }
}

module.exports = UserHomeController