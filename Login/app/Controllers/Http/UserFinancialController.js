'use strict'

const UserFinancial = use('App/Models/UserFinancial')
class UserFinancialController {
    list = [];
    listOp = [];
    listOpRes = [];
    lisABC = ['a', 'b', 'c']
    iOp = 0;
    minOp = 0;
    maxOp = 0;

    async store({ request, response }) {
        const UserData = request.only(['user_id_financial', 'salary', 'other_income', 'monthly_expenses', 'financial_expenses'])
        const user = await UserFinancial.create(UserData)

        return response.created({
            status: true,
            data: user
        })
    }
    get formatter() {
        return formatters.JsonApi
    }

    async getFinancial({ request, response }) {
        const { email } = request.only(['email'])

        const Database = use('Database')
        const posts = await Database
            .table('users')
            .innerJoin('user_financials', 'user_id', 'user_id_financial')
            .where('email', email)

        return posts
    }

    async getSalary({ request, response }) {
        const { email } = request.only(['email'])

        const Database = use('Database')
        const posts = await Database
            .table('users')
            .innerJoin('user_financials', 'user_id', 'user_id_financial')
            .where('email', email)

        return posts
    }

    async getFinancialRange({ request, response }) {
        const { salary, range } = request.only(['salary', 'range'])
        var response = this.optionsRange(salary, range);
        return response
    }

    optionsRange(salary, range) {
        var min = Math.ceil(salary + range)
        var max = Math.floor(salary - range)

        var a = Math.floor(Math.random() * (max - min) + min)

        var b = Math.ceil(Math.random() * (max + min) + min)
        this.list.push({
            "option": 'a',
            'min': this.positive(a),
            'max': this.positive(b),
        });

        max = Math.floor(range - a)
        min = Math.ceil(range + b)

        this.list.push({
            "option": 'b',
            'min': this.positive(max),
            'max': this.positive(a),
        });

        this.list.push({
            "option": 'c',
            'min': this.positive(b),
            'max': this.positive(min),
        });


        for (var i = 0; i < 3; i++) {
            if ((this.list[i]['min'] < salary && this.list[i]['max'] > salary) ||
                (this.list[i]['min'] > salary && this.list[i]['max'] < salary)) {

                this.list.push({
                    "option": this.list[i]['option'],
                    'min': this.positive(this.list[i]['min']),
                    'max': this.positive(this.list[i]['max']),
                });

                return this.list
            }
        }
        this.optionsRange(salary, range)
    }

    positive(num) {
        if (num >= 0) { return num } else { return 0 }
    }

    async getFinancialRangeProcedure({ request, response }) {
        const { salary } = request.only(['salary'])
        var response = this.optionsProcedure(salary);
        return response
    }
    optionsProcedure(salary) {
        let x = salary;
        let fn = this.firstNumber(x)
        let tot = this.lengthNumber(x)
        var num = this.baseNumber(fn, tot)
        this.options(parseInt(fn), parseInt(tot), parseInt(fn), parseInt(tot));
        this.randomOp(2);
        this.answerdOp(salary)
        return this.listOpRes;
    }

    answerdOp(salary) {

        for (var i = 0; i < 3; i++) {
            if ((this.listOpRes[i]['min'] < salary && this.listOpRes[i]['max'] > salary)) {

                this.listOpRes.push({
                    "option": this.listOpRes[i]['option'],
                    'min': this.listOpRes[i]['min'],
                    'max': this.listOpRes[i]['max'],
                });

                return this.listOpRes;
            }
        }

    }

    randomOp(num) {
        var i = num;
        var aleatorio = Math.round(Math.random() * i);

        this.listOpRes.unshift({
            "option": this.lisABC[i],
            "min": this.listOp[aleatorio]["min"],
            "max": this.listOp[aleatorio]["max"]
        });
        this.listOp.splice(aleatorio, 1);

        if (i <= 0) {
            return this.listOpRes;
        } else {

            i--;
            this,
            this.randomOp(i);
        }
    }

    options(x1, y1, x2, y2) {
        this.iOp = this.iOp + 1;

        if (x1 - 1 == 0) {
            x1 = 9;
            y1 = y1 - 1;
        } else {
            x1 = x1 - 1;
        }
        this.minOp = this.baseNumber(x1.toString(), y1);

        if (x2 + 1 > 9) {
            x2 = 1;
            y2 = y2 + 1;
            this.maxOp = this.baseNumber(x2.toString(), y2);
            y2 = y2 - 1;
        } else {
            x2 = x2 + 1;
            this.maxOp = this.baseNumber(x2.toString(), y2);
        }


        this.listOp.push({
            "min": this.minOp,
            "max": this.maxOp
        });


        if (this.iOp == 1) {
            if (x2 - 3 < 0) {
                var aux = 1 + x2.toString();
                this.options(x1 - 1, y1, parseInt(aux) - 4, y2)
            } else {
                this.options(x1 - 1, y1, x2 - 4, y2)
            }
        } else
        if (this.iOp == 2) {
            this.options(x1 + 5, y1, x2 + 4, y2)
        } else
        if (this.iOp == 3) {
            return this.listOp;
        }


    }

    lengthNumber(x) {
        return x.toString().split('').length;
    }
    firstNumber(x) {
        var aux = x.toString().split('');
        return aux[0];
    }

    baseNumber(x, y) {
        var aux = x;
        for (var i = 0; i < y - 1; i++) {
            aux = aux + 0;
        }
        return aux;
    }





    async getFinancialAll({ request, response }) {
        const Database = use('Database')
        const posts = await Database
            .table('user_financials').select('*')
        return posts
    }
}

module.exports = UserFinancialController