'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserHomeSchema extends Schema {
    up() {
        this.create('user_homes', (table) => {
            //table.increments()
            table.integer('user_id_home', 15).references('user_id').inTable('users').unique()
            table.string('department', 254).notNullable()
            table.string('city', 254).notNullable()
            table.string('neighborhood', 254).notNullable()
            table.string('address', 254).notNullable()
            table.timestamps()
        })
    }

    down() {
        this.drop('user_homes')
    }
}

module.exports = UserHomeSchema