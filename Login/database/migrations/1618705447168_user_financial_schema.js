'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserFinancialSchema extends Schema {
    up() {
        this.create('user_financials', (table) => {
            //table.increments()
            table.integer('user_id_financial', 15).references('user_id').inTable('users').unique()
            table.integer('salary', 254).notNullable()
            table.integer('other_income', 254).notNullable()
            table.integer('monthly_expenses', 254).notNullable()
            table.integer('financial_expenses', 254).notNullable()
            table.timestamps()
        })
    }

    down() {
        this.drop('user_financials')
    }
}

module.exports = UserFinancialSchema