'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SaveLoginSchema extends Schema {
    up() {
        this.create('save_logins', (table) => {
            table.increments()
            table.string('email_login', 15).references('email').inTable('users')
            table.string('password', 15).notNullable()
            table.timestamps()
        })
    }

    down() {
        this.drop('save_logins')
    }
}

module.exports = SaveLoginSchema