'use strict'

const Route = use('Route')

Route.post('/log', 'SaveLoginController.login')
Route.get('/getDataAll', 'UserFinancialController.getFinancialAll')
Route.post('/getData', 'UserFinancialController.getFinancial')
Route.post('/getRangeSalary', 'UserFinancialController.getFinancialRange')
Route.post('/getRangeSalaryPro', 'UserFinancialController.getFinancialRangeProcedure')


Route.resource('users', 'UserController')
    .apiOnly()
    .validator(new Map([
        [
            ['users.login'],
            ['Users']
        ]
    ]))

Route.resource('user_financial', 'UserFinancialController')
    .apiOnly()
    .validator(new Map([
        [
            ['users.financial'],
            ['UsersFinancial']
        ]
    ]))

Route.resource('user_home', 'UserHomeController')
    .apiOnly()
    .validator(new Map([
        [
            ['users.home'],
            ['UsersHome']
        ]
    ]))

Route.resource('user_login', 'SaveLoginController')
    .apiOnly()
    .validator(new Map([
        [
            ['users.login'],
            ['UsersLogin']
        ]
    ]))